#! /usr/bin/env python
"""
Author: Jeremy M. Stober
Program: __INIT__.PY
Date: Wednesday, May 23 2012
"""

from pygmm.gmm import GMM
from pygmm.normal import Normal
