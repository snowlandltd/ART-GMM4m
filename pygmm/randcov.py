#! /usr/bin/env python
"""
Author: Jeremy M. Stober
Program: RANDCOV.PY
Date: Thursday, October 27 2011
Description: Generate random cov matrix in numpy.
"""

import numpy as np



def gencov(n):
    S = np.random.randn(n,n)
    S = np.dot(S.transpose(), S)
    s = np.sqrt(np.diag(S))
    t = np.diag(1.0/s)
    C = np.dot(np.dot(t,S),t)
    return C

if __name__ == '__main__':
    print(gencov(2))
